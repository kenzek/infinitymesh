﻿using AutoMapper;
using Model.Models;
using Presentation.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Mappings
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "DomainToViewModelMappings"; }
        }

        public ViewModelToDomainMappingProfile()
        {


        }
    }
}