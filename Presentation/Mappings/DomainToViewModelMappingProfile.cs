﻿using AutoMapper;
using Model.Models;
using Presentation.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Mappings
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "DomainToViewModelMappings"; }
        }

        public DomainToViewModelMappingProfile()
        {
            CreateMap<User, IdentityUser>()
                 .ForMember(x => x.BlogCount, y => y.MapFrom(src => src.Blogs.Count));
        }


    }
}