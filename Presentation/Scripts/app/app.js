﻿

$(document).ready(function () {

    var app = $.sammy("#app", function () {
        var main = this.$element();

        this.get('/', function (context) {

            $.ajax({
                url: 'Account/Login',
                success: function (item) {
                    main.html(item);
                }
            })
        });

        this.post('/login', function (context) {
            fields = this.params;
            var serializedFields = $("#login").serialize();

            $.post(
                'Account/Login',
                serializedFields,
            ).done(function (data) {
                $(main).html(data);
                getUsers();
            })
                .fail(function (response) {
                    $(main).html(response.responseText);
                });
        });

        this.get('/users:filter', function (context) {
            var filter = this.params['filter'];
            $.get(
                '/Users?page=1&count=5&filter=' + filter
            ).done(function (data) {

                $(main).find("#main").html(data);

            });

        });

        function getUsers() {

            $.get(
                '/Users?page=1&count=5&filter='
            ).done(function (data) {

                $(main).find("#main").html(data);

            });

        }

        this.get('/test', function (context) {

            $.ajax({
                url: 'home/test',
                success: function (item) {
                    main.html(item);
                },
                error(xhr, status, error) {
                    main.html(xhr.responseText);
                }

            })
        });
    });

    app.run('/');
});