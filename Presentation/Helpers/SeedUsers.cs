﻿using Data.Infrastructure;
using Data.Repositories;
using Microsoft.AspNet.Identity;
using Presentation.Identity;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Helpers
{
    public class SeedUsers
    {
        public static void Seed()
        {
            DbFactory dbFactory = new DbFactory();
            UserRepository userRepository = new UserRepository(dbFactory);
            UnitOfWork unitOfWork = new UnitOfWork(dbFactory);
            UsersService usersService = new UsersService(userRepository, unitOfWork);
            UserStore userStore = new UserStore(usersService);
            UserManager<IdentityUser, int> userManager = new UserManager<IdentityUser, int>(userStore);

            userManager.Create(new IdentityUser()
            {
                UserName="admin",
                Age=20,
                Email="email@email.com",
            }, "admin");

            userManager.Create(new IdentityUser()
            {
                UserName = "ken",
                Age = 20,
                Email = "email@sad.com",
            }, "admin");

            userManager.Create(new IdentityUser()
            {
                UserName = "ken33",
                Age = 20,
                Email = "email@sad.com",
            }, "admin");

            userManager.Create(new IdentityUser()
            {
                UserName = "john",
                Age = 20,
                Email = "john@sad.com",
            }, "admin");
        }

    }
}