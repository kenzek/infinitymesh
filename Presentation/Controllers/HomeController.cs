﻿using Data.Infrastructure;
using Data.Repositories;
using Microsoft.AspNet.Identity;
using Presentation.Identity;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Presentation.Controllers
{
    public class HomeController : Controller
    {



        // GET: Home
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult Main()
        {
            return PartialView("Main");
        }

        [Authorize]
        public ActionResult Test()
        {
            return PartialView();
        }
    }
}