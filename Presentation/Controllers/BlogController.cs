﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using PagedList;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Presentation.Controllers
{
    public class BlogController : Controller
    {

        IBlogsService blogsService;
        IUnitOfWork unitOfWork;
        IBlogRepository blogsRepository;
        IDbFactory dbFactory;

        public BlogController()
        {
            dbFactory = new DbFactory();
            unitOfWork = new UnitOfWork(dbFactory);
            blogsRepository = new BlogRepository(dbFactory);

            blogsService = new BlogsService(blogsRepository, unitOfWork);
        }
        // GET: Blog
        [Authorize]
        public ActionResult Index(int page, int size, int userId, string dateFrom, string dateTo, string filter = "")
        {
            DateTime _dateFrom = DateTime.MinValue;
            DateTime _dateTo = DateTime.Now;

            DateTime.TryParse(dateFrom, out _dateFrom);
            DateTime.TryParse(dateTo, out _dateTo);

            ViewBag.DateFrom = dateFrom;
            ViewBag.DateTo = dateTo;
            ViewBag.Filter = filter;

            List<Blog> blogs = blogsService.FindByUserPublishDateAndTitle(userId, filter, _dateFrom, _dateTo);
            IPagedList<Blog> pageListBlog = blogs.ToPagedList(page, size);


            return PartialView("Index", pageListBlog);


        }



        [Authorize]
        public ActionResult GetBlogs(int page, int size, int userId, string dateFrom, string dateTo, string filter = "")
        {
            DateTime _dateFrom = DateTime.MinValue;
            DateTime _dateTo = DateTime.Now;

            DateTime.TryParse(dateFrom, out _dateFrom);
            if (!DateTime.TryParse(dateTo, out _dateTo))
            {
                _dateTo = DateTime.Now;
            }

            

            ViewBag.DateFrom = dateFrom;
            ViewBag.DateTo = dateTo;
            ViewBag.Filter = filter;

            List<Blog> blogs = blogsService.FindByUserPublishDateAndTitle(userId, filter, _dateFrom, _dateTo);
            IPagedList<Blog> pageListBlog = blogs.ToPagedList(page, size);


            return PartialView("List", pageListBlog);



        }

    }
}