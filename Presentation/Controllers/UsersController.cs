﻿using AutoMapper;
using Data.Infrastructure;
using Data.Repositories;
using Presentation.Identity;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model.Models;
namespace Presentation.Controllers
{
    public class UsersController : Controller
    {
        IUsersService usersService;
        IUnitOfWork unitOfWork;
        IUserRepository usersRepository;
        IDbFactory dbFactory;
       
        public UsersController()
        {
            dbFactory = new DbFactory();
            unitOfWork = new UnitOfWork(dbFactory);
            usersRepository = new UserRepository(dbFactory);

            usersService = new UsersService(usersRepository, unitOfWork);
        }

        // GET: Users
        public ActionResult Index(int page, int count, string filter = "")
        {

            List<IdentityUser> users = Mapper.Map<List<User>, List<IdentityUser>>(usersService.FindByNameOrEmail(filter, page - 1, count));
            return PartialView("Index", users);
        }

        public ActionResult Profile(int userId)
        {
            IdentityUser user = Mapper.Map<User, IdentityUser>(usersService.FindById(userId));
            return PartialView("Profile", user);
        }


    }
}