﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Presentation.Identity;
using Presentation.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Presentation.Controllers
{
    public class AccountController : Controller
    {

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public ActionResult Login()
        {
            return PartialView("Login");
        }

        [HttpPost]
        public async Task<ActionResult> Login(LoginVM login)
        {

            if (ModelState.IsValid)
            {
                IdentityUser identityUser = await UserManager.FindAsync(login.UserName, login.Password);
                if (identityUser != null)
                {
                    SignInManager.SignIn(identityUser, false, true);
                    ViewBag.UserId = identityUser.Id;
                    ViewBag.UserName = identityUser.UserName;

                    return RedirectToAction("Main", "Home");
                }
                else
                {
                    ViewBag.LoginFailed = "Incorrect username or password";

                    return PartialView("Login", login);
                }

            }
            else
            {
                return PartialView("Login", login);
            }
        }

        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();

            return RedirectToAction("Index", "Home");
        }
    }
}