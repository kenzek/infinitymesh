﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Identity
{
    public class IdentityUser : IUser<int>
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string PasswordHash { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }
        public int? BlogCount { get; set; }
        public string _stamp { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string SecurityStamp { get { return Guid.NewGuid().ToString(); } set { _stamp = Guid.NewGuid().ToString(); } }
    }
}