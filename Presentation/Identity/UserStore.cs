﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Model.Models;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Presentation.Identity
{
    public class UserStore : IUserPasswordStore<IdentityUser, int>, IUserLockoutStore<IdentityUser, int>, IUserTwoFactorStore<IdentityUser, int>
    {
        IUsersService usersService;


        public UserStore(IUsersService _usersService)
        {
            usersService = _usersService;

        }

        public Task CreateAsync(IdentityUser user)
        {
            User _user = Mapper.Map<IdentityUser, User>(user);

            return usersService.CreateAsync(_user);
        }

        public Task DeleteAsync(IdentityUser user)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {

        }

        public Task<IdentityUser> FindByIdAsync(int userId)
        {
            User user = usersService.FindById(userId);
            return Task.FromResult(Mapper.Map<User, IdentityUser>(user));
        }

        public Task<IdentityUser> FindByNameAsync(string userName)
        {
            User user = usersService.FindByName(userName);
            return Task.FromResult(Mapper.Map<User, IdentityUser>(user));
        }

        public Task<int> GetAccessFailedCountAsync(IdentityUser user)
        {
            return Task.FromResult(0);
        }

        public Task<bool> GetLockoutEnabledAsync(IdentityUser user)
        {
            return Task.FromResult(false);
        }

        public Task<DateTimeOffset> GetLockoutEndDateAsync(IdentityUser user)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetPasswordHashAsync(IdentityUser user)
        {
            return Task.FromResult(user.PasswordHash);
        }

        public Task<bool> GetTwoFactorEnabledAsync(IdentityUser user)
        {
           return Task.FromResult(false);
        }

        public Task<bool> HasPasswordAsync(IdentityUser user)
        {
            throw new NotImplementedException();
        }

        public Task<int> IncrementAccessFailedCountAsync(IdentityUser user)
        {
            throw new NotImplementedException();
        }

        public Task ResetAccessFailedCountAsync(IdentityUser user)
        {
            throw new NotImplementedException();
        }

        public Task SetLockoutEnabledAsync(IdentityUser user, bool enabled)
        {
            throw new NotImplementedException();
        }

        public Task SetLockoutEndDateAsync(IdentityUser user, DateTimeOffset lockoutEnd)
        {
            throw new NotImplementedException();
        }

        public Task SetPasswordHashAsync(IdentityUser user, string passwordHash)
        {
            user.PasswordHash = passwordHash;
            return Task.CompletedTask;
        }

        public Task SetTwoFactorEnabledAsync(IdentityUser user, bool enabled)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(IdentityUser user)
        {
            throw new NotImplementedException();
        }
    }
}