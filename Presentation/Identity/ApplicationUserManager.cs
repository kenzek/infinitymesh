﻿using Data.Infrastructure;
using Data.Repositories;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Identity
{
    public class ApplicationUserManager : UserManager<IdentityUser, int>
    {
        public ApplicationUserManager(IUserStore<IdentityUser, int> store)
            : base(store)
        {
            PasswordValidator = new MinimumLengthValidator(5);
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            DbFactory dbFactory = new DbFactory();
            UserRepository userRepository = new UserRepository(dbFactory);
            UnitOfWork unitOfWork = new UnitOfWork(dbFactory);
            UsersService usersService = new UsersService(userRepository, unitOfWork);
            UserStore userStore = new UserStore(usersService);
            UserManager<IdentityUser, int> userManager = new UserManager<IdentityUser, int>(userStore);


            var manager = new ApplicationUserManager(userStore);
            return manager;
        }
    }

}