﻿using Model.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class BlogsInitializer : DropCreateDatabaseIfModelChanges<BlogContext>
    {
        protected override void Seed(BlogContext context)
        {
            List<User> users = GetUsers();
            context.Users.AddRange(users);
            context.SaveChanges();
            context = new BlogContext();

            List<Blog> blogs = GetBlogs(users);
            context.Blogs.AddRange(blogs);
            context.SaveChanges();
        }

        private List<Blog> GetBlogs(List<User> users)
        {
            List<Blog> blogs = new List<Blog>();
            blogs.Add(new Blog()
            {
                Content = "Lorem ipsum dolor sit amet, tota sale ad mel, justo possit dissentiet eos ea," +
                " in qui esse facete. Delenit rationibus in sit, unum numquam feugiat in duo, sea et amet definitionem." +
                " No suas ignota eum. Ex quaestio patrioque vix, ut quo voluptua tacimates reformidans." +
                " Odio appetere duo ne, suscipit iudicabit consequat eam no. Nam cu hinc mundi, at vel utinam" +
                " oblique assentior.",
                PublishDate = new DateTime(2018, 5, 22),
                Summary = "Lorem ipsum dolor sit amet, tota sale ad mel, justo possit dissentiet eos ea..",
                Title = "Lorem ipsum",
                UserId = users.ElementAt(1).Id
            });

            blogs.Add(new Blog()
            {
                Content = "Facilisi referrentur reprehendunt no mei. Prima menandri delicata per ea." +
                " Unum putent prompta vis no, ea munere habemus dissentias mel. Elitr interpretaris eos ut." +
                " Eam te quot partem, sed utinam legimus an." +
                " Eum cu oratio omnium perpetua.",
                PublishDate = new DateTime(2018, 5, 20),
                Summary = "Facilisi referrentur reprehendunt no mei. Prima menandri delicata per ea.",
                Title = "Facilisi referrentur",
                UserId = users.ElementAt(1).Id
            });

            blogs.Add(new Blog()
            {
                Content = "Agam labitur vix at, veritus insolens quo eu," +
                " sit tota labore singulis at. Cu vim purto solet nonumy, aperiri intellegat dissentiet sed ex." +
                " Congue partem placerat te vis, mei at alii hinc efficiantur. Exerci delicatissimi te usu," +
                " nec errem minimum menandri eu. Eu ius nullam antiopam mnesarchum. Per unum debitis accommodare ad." +
                " Novum iusto ad sit.",
                PublishDate = new DateTime(2018, 5, 25),
                Summary = "Facilisi referrentur reprehendunt no mei. Prima menandri delicata per ea.",
                Title = "Facilisi referrentur",
                UserId = users.ElementAt(2).Id
            });


            blogs.Add(new Blog()
            {
                Content = "The Fairly Random text generator generates realistic seeming random text from any" +
                " given source text." +
                " To do this it analyzes a source text that you provide." +
                " It measures overall word length and frequency, and lets you review the individual word" +
                "s of the original text. It also immediately creates a random text based on your source text. " +
                "If you want, you can then use the various options to tell the text generator which of the words to use (or not to use) and what the resulting text should look like (in terms of sentences, paragraphs, headers et cetera).",
                PublishDate = new DateTime(2018, 5, 15),
                Summary = "The Fairly Random text generator generates realistic seeming random text from any",
                Title = "The Fairly Random text generator",
                UserId = users.ElementAt(1).Id
            });

            blogs.Add(new Blog()
            {
                Content = "While random text is mostly intended to just fill up the blank spaces, for many practical purposes you essentially want the look and feel of the dummy text you are going to use to be as close as possible to the real content that will eventually replace it. Considering that different language, writing style and substance of text result in an endless variety of different word combinations and visual impressions that a piece of text can have and make, it is always a plus to have it use the right kind of words and look like the final product as much as possible. Whether you are working in the technical, lifestyle, news or fantasy business,"
       
,
                PublishDate = new DateTime(2018, 5, 20),
                Summary = "While random text is mostly intended to just fill up the blank spaces, for many practical p",
                Title = "Dummy text, fake text, placeholder text. why make it look nice?",
                UserId = users.ElementAt(1).Id
            });


            blogs.Add(new Blog()
            {
                Content = "You might be surprised how much mood or athmosphere even a totally random text can carry, while a fairly random text does this better ofcourse ;-). Granted, when you really get down to reading it, there won't be much meaningful coherence in any of it, but... regardless of the fact that random text makes no real sense, it does automatically get your mind working. Your brain will in fact try hard to understand it anyway, because out of habit it assumes that words in a text should mean something, or at least anything but nothing! You see, the brain launches itself into ‘I-must-understand-this’ mode when confronted with random text, trying every possible way in which it could still extract some sensible meaning from those random words, much like trying to solve a puzzle. Just try it. Reading random text for a few minutes, word for word, is very tiring. On the upside, reading a little bit, or just skimming over a random text like most people will do, makes your mind strongly touch upon a certain global idea of “subject matter” without its focus getting diverted by any actual meaningful details, while what the random text does do is automatically fill the readers mind with questions at the same time. The more your strongly your source text is written on a specific subject and uses a specific group of words, the stronger this effect will be. And this is exactly what you want your Fairly Random text to accomplish: it is there to provide a general mood, to hook interest and to look like the real thin"

,
                PublishDate = new DateTime(2018, 3, 25),
                Summary = "You might be surprised how much mood or athmosphere even a totally random",
                Title = "On a sidenote...",
                UserId = users.ElementAt(1).Id
            });


            blogs.Add(new Blog()
            {
                Content = "Having your dummy text support your design content-wise looks way more convincing than just some unrelated, repetitive placeholder text that is not even in the right language and which just “looks” wrong, but those days are now forever gone! The Fairly Random text generator happily sets to work with anything you want that has the right kind of words and is in the right language! Just copy a piece about gardening, some recipes, a game review, notes from a business meeting, a fantasy story, or anything... you name it. Set some options, copy the result and start working with adequate and realistic looking fake text.	"

,
                PublishDate = new DateTime(2018, 4, 25),
                Summary = "Having your dummy text support your design content-wise looks way",
                Title = "For designers...",
                UserId = users.ElementAt(1).Id
            });



            blogs.Add(new Blog()
            {
                Content = "Analyze your (or someone else’s) writing style! Input a story and see a clean overview of which words are used and how often they are used. Did you like something you read? Just input it here to get a quick idea of which words the writer relies on to create a certain mood or context, see if a certain text tends toward long or short general word usage and see how rich, or boring, a text is in use of unique words. Or just feed the text generator a themed word set, let it destroy a beautiful poem or have it rewrite the plans for your honeymoon... and then read the result for inspiration and adventure ;-).	"

,
                PublishDate = new DateTime(2018, 4, 5),
                Summary = "Analyze your (or someone else’s) writing style! Input a story and see a clean overview of which words are used and how often they are used. ",
                Title = "For writers...",
                UserId = users.ElementAt(1).Id
            });

            return blogs;
        }

        private List<User> GetUsers()
        {
            List<User> users = new List<User>();

            users.Add(new User()
            {
                UserName = "admin",
                PasswordHash = HashPassword("admin"),
                Age = 20,
                Email = "admin@admin.com",
                FirstName = "Admin",
                LastName = "Admin"

            });

            users.Add(new User()
            {
                UserName = "ken",
                PasswordHash = HashPassword("admin"),
                Age = 20,
                Email = "ken@ken.com",
                FirstName = "Ken",
                LastName = "Zek"

            });


            users.Add(new User()
            {
                UserName = "john",
                PasswordHash = HashPassword("admin"),
                Age = 20,
                Email = "john@admin.com",
                FirstName = "John",
                LastName = "Doe"

            });

            users.Add(new User()
            {
                UserName = "user",
                PasswordHash = HashPassword("user"),
                Age = 20,
                Email = "user@admin.com",
                FirstName = "User",
                LastName = "User"

            });

            return users;
        }

        public static string HashPassword(string password)
        {
            byte[] salt;
            byte[] buffer2;
            if (password == null)
            {
                throw new ArgumentNullException("password");
            }
            using (Rfc2898DeriveBytes bytes = new Rfc2898DeriveBytes(password, 0x10, 0x3e8))
            {
                salt = bytes.Salt;
                buffer2 = bytes.GetBytes(0x20);
            }
            byte[] dst = new byte[0x31];
            Buffer.BlockCopy(salt, 0, dst, 1, 0x10);
            Buffer.BlockCopy(buffer2, 0, dst, 0x11, 0x20);
            return Convert.ToBase64String(dst);
        }
    }
}
