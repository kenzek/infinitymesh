﻿using Model.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Configuration
{
    public class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            ToTable("User");
            Property(x => x.Id).IsRequired();
            Property(x => x.Email).IsRequired().HasMaxLength(50);
            Property(x => x.UserName).IsRequired().HasMaxLength(20);
            HasMany(x => x.Blogs).WithRequired(x => x.User).HasForeignKey(x => x.UserId);
        }
    }
}
