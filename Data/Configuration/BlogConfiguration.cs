﻿using Model.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Configuration
{
    public class BlogConfiguration : EntityTypeConfiguration<Blog>
    {
        public BlogConfiguration()
        {
            ToTable("Blog");
            Property(x => x.Id).IsRequired();
            Property(x => x.Title).IsRequired().HasMaxLength(64);
            Property(x => x.Summary).IsRequired().HasMaxLength(350);
            Property(x => x.Content).IsRequired().HasMaxLength(3500);
            Property(x => x.PublishDate).IsRequired();
          
        }
    }
}
