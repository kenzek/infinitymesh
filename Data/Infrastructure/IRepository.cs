﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Infrastructure
{
    public interface IRepository<TEntity> where TEntity : class
    {
        // Marks an entity as new
        void Add(TEntity entity);
        // Marks an entity as modified
        void Update(TEntity entity);
        // Get an entity by int id
        TEntity GetById(int id);
        // Gets entities using delegate


    }
}
