﻿using Data.Infrastructure;
using Model.Models;
using Ninject;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        [Inject]
        public UserRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public User FindById(int id)
        {
            return this.DbContext.Users.Where(x => x.Id == id).FirstOrDefault();

        }

        public User FindByUserName(string username)
        {
            return this.DbContext.Users.Where(x => x.UserName == username).FirstOrDefault();
        }

        public IEnumerable<User> GetMany(Expression<Func<User, bool>> where, int count, int index)
        {
            return this.DbContext.Users.Include("Blogs").Where(where).OrderBy(x => x.UserName).Skip(index * count).Take(count);
        }
    }

    public interface IUserRepository : IRepository<User>
    {

        User FindByUserName(string username);
        User FindById(int id);
        IEnumerable<User> GetMany(Expression<Func<User, bool>> where, int count, int index);
    }
}
