﻿using Data.Infrastructure;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class BlogRepository : Repository<Blog>, IBlogRepository
    {
        public BlogRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<Blog> GetMany(Expression<Func<Blog, bool>> where)
        {
            return this.DbContext.Blogs.Where(where).OrderBy(x => x.PublishDate);
        }
    }

    public interface IBlogRepository : IRepository<Blog>
    {
        IEnumerable<Blog> GetMany(Expression<Func<Blog, bool>> where);

    }
}
