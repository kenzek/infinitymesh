﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services
{
    public class BlogsService : IBlogsService
    {
        IBlogRepository blogRepository;
        IUnitOfWork unitOfWork;

        public BlogsService(IBlogRepository _blogRepository, IUnitOfWork _unitOfWork)
        {
            blogRepository = _blogRepository;
            unitOfWork = _unitOfWork;
        }

        public List<Blog> FindByUserPublishDateAndTitle(int userId, string title, DateTime dateFrom, DateTime dateTo)
        {
            return blogRepository.GetMany(x => x.UserId == userId &&
                                    x.Title.Contains(title) &&
                                    x.PublishDate >= dateFrom &&
                                    x.PublishDate <= dateTo).ToList();
        }
    }

    public interface IBlogsService
    {
        List<Blog> FindByUserPublishDateAndTitle(int userId, string title, DateTime dateFrom, DateTime dateTo);
    }
}
