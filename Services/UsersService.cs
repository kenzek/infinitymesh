﻿using Data.Infrastructure;
using Data.Repositories;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class UsersService : IUsersService
    {
        IUserRepository userRepository;
        IUnitOfWork unitOfWork;

        public UsersService(IUserRepository _userRepository, IUnitOfWork _unitOfWork)
        {
            userRepository = _userRepository;
            unitOfWork = _unitOfWork;
        }

        public Task CreateAsync(User user)
        {
            userRepository.Add(user);
            return unitOfWork.CommitAsync();
        }

        public User FindById(int userId)
        {
            return userRepository.FindById(userId);
        }

        public User FindByName(string username)
        {
            return userRepository.FindByUserName(username);
        }

        public List<User> FindByNameOrEmail(string filter, int page, int count)
        {
            return userRepository.GetMany(x => x.UserName.Contains(filter) || x.Email.Contains(filter), count, page).ToList();
        }
    }

    public interface IUsersService
    {
        Task CreateAsync(User user);
        User FindById(int userId);
        User FindByName(string username);
        List<User> FindByNameOrEmail(string filter, int page, int count);
    }
}
